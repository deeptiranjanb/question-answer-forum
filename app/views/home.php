<body>
    <!-- Navigation -->
    <nav class=" navbar navbar-expand-lg navbar-light bg-light shadow sticky-top">
        <div class="container">
            <a class="navbar-brand" href="/home">QA Forum</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active" id="home">
                        <a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item" id="service">
                        <a class="nav-link" href="#services" onclick="service()">Services</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/home/sign_in">Sign in</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/signup">Sign up</a>
                    </li>


                </ul>
            </div>
        </div>
    </nav>

    <!-- Full Page Image Header with Vertically Centered Content -->
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 text-center ">
                    <h1 class="font-weight-light">Question Answering Forum</h1>
                    <p class="lead">Sign in to start asking questions</p>
                </div>
            </div>
        </div>
    </header>

    <!-- Page Content -->
    <section class="py-5">
        <div class="container" id="services">
            <h2 class="font-weight-light">Services</h2>
            <p>Currently we only answer the questions you asked. More services coming soon...</p>
        </div>
    </section>