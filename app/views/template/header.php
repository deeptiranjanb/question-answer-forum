<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-signin-client_id"
        content="992378561159-8cmqpsqkjnttfk9v26a4n3f9ech04ngk.apps.googleusercontent.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>QA Forum</title>

    <!-- Favicon -->
    <link rel="icon" href="favicon.png" type="image/png" sizes="32x32" />

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <link rel="stylesheet" href="/public/stylesheet/home.css">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>

    <script>
    function signOut() {
        console.log('hi');

        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function() {
            console.log('User signed out.');
            window.location = 'home/sign_out';
        });
    }

    function onLoad() {
        gapi.load('auth2', function() {
            gapi.auth2.init();
        });

    }

    function service() {
        $("#search").removeClass("active");
        $("#home").removeClass("active");
        $("#service").addClass("active");
    }
    </script>
</head>