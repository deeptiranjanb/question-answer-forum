<?php
require __DIR__ . "../../../controllers/csrf.php";
?>

<body class="d-flex flex-column">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow sticky-top">
        <div class="container">
            <a class="navbar-brand" href="/home">QA Forum</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item" id="home">
                        <a class="nav-link" href="/">Home

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item" id="service">
                        <a class="nav-link" href="home/#services" onclick="service()">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/signin">Sign in</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-xl-9 mx-auto">
                <div class="card card-signin flex-row my-5">
                    <div class="card-img-left d-none d-md-flex">
                        <!-- Background image for card set in CSS! -->
                    </div>
                    <div class="card-body">
                        <h5 class="card-title text-center">Register</h5>
                        <form class="form-signin" method="post" action="/signup/sign_up">
                            <div class="form-label-group">
                                <input type="text" id="inputName" name="name" class="form-control" placeholder="Name" required autofocus>
                                <label for="inputName">Name</label>
                                <span class="error"><?php echo $nameErr; ?></span>
                            </div>
                            <div class="form-label-group">
                                <input type="text" id="username" name="username" class="form-control" placeholder="Username" required>
                                <label for="username">Username</label>
                                <div class="error" id="usernameErr"><?php echo $userNameErr; ?></div>
                            </div>

                            <div class="form-label-group">
                                <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required>
                                <label for="inputEmail">Email address</label>
                                <span class="error"><?php echo $emailErr; ?></span>
                            </div>

                            <hr>

                            <div class="form-label-group">
                                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
                                <label for="inputPassword">Password</label>
                            </div>

                            <div class="form-label-group">
                                <input type="password" id="inputConfirmPassword" name="cpassword" class="form-control" placeholder="Password" required>
                                <label for="inputConfirmPassword">Confirm password</label>
                                <span class="error"><?php echo $passwordErr; ?></span>
                            </div>

                            <input type="hidden" name="token" value="<?php echo $token; ?>">

                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Register</button>
                            <hr class="my-4">
                            <a class="btn btn-lg btn-danger btn-block text-uppercase" href="/signin">Sign In instead</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>