<body class="d-flex flex-column">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow sticky-top">
        <div class="container">
            <a class="navbar-brand" href="#">QA Forum</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarResponsive">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active" id="search">
                        <a class="nav-link" href="/question">Search
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/history">History</a>
                    </li>
                    <li class="nav-item" id="service">
                        <a class="nav-link" href="#services" onclick="service()">Services</a>
                    </li>
                    <li class="nav-item avatar dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg"
                                class="rounded-circle z-depth-0" style="height:30px" alt="avatar image">
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                            aria-labelledby="navbarDropdownMenuLink-55">
                            <a class="dropdown-item" href="#">My account</a>
                            <a class="dropdown-item" onclick="signOut()">Sign out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class='container p-2' id="page-content">
        <div class="container" id="page-content">
            <div class="card border-2 shadow my-5">
                <div class="card-body-lg p-5">
                    <h1><?php echo $question . '?' ?></h1>
                    <hr class="my-4">
                    <p><?php echo $answer; ?></p>

                    <p style="font-weight:bold">Not what you were looking for. Check these links below to know more:
                    </p>
                    <br />
                    <?php foreach ($url as $link) { ?>
                    <a class="pb-2" style="display:table; overflow:auto; word-break: break-all;"
                        rel="noopener noreferrer" target="_blank" href="<?php echo $link; ?>"><?php echo $link; ?></a>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>