<?php

class signup extends Controller
{

    /*
     * http://localhost/signup
     */
    function Index()
    {

        $this->view('signup/header');
        $this->view('signup/index');
        $this->view('template/footer');
    }
    /*
* http://localhost/signup/sign_up
*/
    function Sign_up()
    {

        if (!empty($_POST["token"])) {
            if (hash_equals($_SESSION["token"], $_POST["token"])) {
                if ($_SERVER["REQUEST_METHOD"] == "POST") {

                    $err = $this->validateDetails($_POST);

                    $this->model('user');
                    if (!($this->user->verifyUsername($_POST['username']))) {

                        $err['userNameErr'] = "username already exits";
                        $this->view('signup/header');
                        $this->view('signup/index', $err);
                        $this->view('template/footer');
                    } else if (!($this->user->verifyEmail($_POST['email']))) {

                        $err['emailErr'] = "email already exits";
                        $this->view('signup/header');
                        $this->view('signup/index', $err);
                        $this->view('template/footer');
                    } else {
                        if (count($err) == 0) {

                            $details = $_POST;
                            unset($details['cpassword']);
                            unset($details['token']);

                            $password = $details["password"];
                            $hash = password_hash($password, PASSWORD_DEFAULT);

                            $details["password"] = $hash;

                            if ($this->user->addUser($details)) {

                                $_SESSION['username'] = $details['username'];
                                header("Location: /myposts");
                            }
                        } else {

                            $this->view('signup/header');
                            $this->view('signup/index', $err);
                            $this->view('template/footer');
                        }
                    }
                }
            }
        }
    }

    function validateDetails($userDetails = [])
    {
        $err = [];

        //name validation
        if (empty($userDetails["name"])) {

            $err['nameErr'] = "Please enter your name";
        } else {

            $name = $this->check_input($userDetails["name"]);
            if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
                $err['nameErr'] = "Only letters and white space allowed";
            }
        }

        //username validation
        if (empty($userDetails["name"])) {
            $err['userNameErr'] = "Please enter a username";
        }

        //Email validation
        if (empty($userDetails["email"])) {
            $err['emailErr'] = "Please enter your email";
        } else {
            $email = $this->check_input($userDetails["email"]);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $err['emailErr'] = "Please enter valid email";
            }
        }

        //password validation
        if (empty($userDetails['password'])) {
            $err['passwordErr'] = "password cannot be empty";
        } else {
            $password = $this->check_input($userDetails["password"]);
            if (!preg_match("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/", $password)) {
                $err['passwordErr'] = "password must contain minimum 8 character, 
                at least one letter and one number";
            }
        }
        if ($userDetails['password'] != $userDetails['cpassword']) {
            $err['passwordErr'] = "passwords do not match";
        }
        return $err;
    }




    /*
* http://localhost/signup/Username_Availability
*/
    function Username_Availability()
    {
        $this->model('user');
        $username = $_POST['username'];

        if ($this->user->verifyUsername($username)) {
            echo true;
        } else {
            echo false;
        }
    }
}
