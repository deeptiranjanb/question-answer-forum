<?php
class Question extends Controller
{
    function Index()
    {
        if (!isset($_SESSION['username'])) {

            header('Location: /signin');
        } else {

            $this->view('template/header');
            $this->view('question/index');
            $this->view('template/footer');
        }
    }
}
