<?php
require __DIR__ . "../../../controllers/csrf.php";
?>

<body class="d-flex flex-column">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow sticky-top">
        <div class="container">
            <a class="navbar-brand" href="/home">QA Forum</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item" id="home">
                        <a class="nav-link" href="/">Home
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item" id="service">
                        <a class="nav-link" href="home/#services" onclick="service()">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/signup">Sign up</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5 ">
                    <div class="card-body form-signin">
                        <h5 class="card-title text-center">Sign In</h5>
                        <form class="form-signin " method="post" action="/signin/sign_in">

                            <div class="form-label-group">
                                <input type="text" id="inputUserName" name="username" class="form-control" placeholder="User Name" required autofocus>
                                <label for="inputUserName">User Name</label>
                            </div>

                            <div class="form-label-group">
                                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
                                <label for="inputPassword">Password</label>
                                <span class="error"><?php echo $error; ?></span>
                            </div>

                            <input type="hidden" name="token" value="<?php echo $token; ?>">
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>


                        </form>
                        <hr class="my-4">

                        <a class="btn btn-lg btn-danger btn-block text-uppercase" href="/signin/sign_up">new user?Sign Up</a>

                        <hr class="my-4">

                        <div class="g-signin2" id="my-signin2" data-onsuccess=" onSignIn" align="center"></div>


                    </div>
                </div>
            </div>
        </div>
    </div>