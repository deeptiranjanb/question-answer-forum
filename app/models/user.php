<?php

/*
 * Every class derriving from Model has access to $this->db
 * $this->db is a PDO object
 * Has a config in /core/config/database.php
 */
class User extends Model
{

    function addUser($userDetails = [])
    {
        try {
            return $this->insert('users', $userDetails);
        } catch (Exception $e) {
            echo "user could not be inserted";
        }
    }


    function verifyUsername($username)
    {
        $conditions = array(
            "select" => ["id"],
            "where" => array(
                "username" => $username
            )
        );

        try {
            $result = $this->select("users", $conditions);
        } catch (Exception $e) {
            echo "user couldnot be selected";
        }

        if (count($result) == 0) {
            return true;
        } else {
            return false;
        }
    }

    function verifyEmail($email)
    {
        $conditions = array(
            "select" => ["id"],
            "where" => array(
                "email" => $email
            )
        );

        try {
            $result = $this->select("users", $conditions);
        } catch (Exception $e) {
            echo "user couldnot be selected";
        }

        if (count($result) == 0) {
            return true;
        } else {
            return false;
        }
    }

    function validateUser($userDetails = [])
    {

        $conditions = array(
            "select" => ["username", "password"],
            "where" => array(
                "userName" => $userDetails['username']
            )
        );

        try {
            $result = $this->select('users', $conditions);
        } catch (Exception $e) {
            echo "no user was selected";
        }

        if (password_verify($userDetails['password'], $result[0]['password'])) {
            return true;
        } else {
            return false;
        }
    }
}
