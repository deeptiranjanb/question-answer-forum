
last_post = 0;
function next() {
    if($(".post4").is(':visible')) {
        last_post += 4;
        $.post("/myposts/next_posts",
            {
                offset: last_post,
            },
            function (data, status) {
                var dataobj = JSON.parse(data);
                for (i = 0; i < 4; i++) {
                    if (dataobj.heading[i]) {
                        id = "#heading" + (i + 1);
                        $(id).html(dataobj.heading[i]);
                        id = "#content" + (i + 1);
                        $(id).html(dataobj.content[i]);
                    } else {

                        cls = ".post" + (i + 1);
                        $(cls).hide();

                    }
                }

            },
        );
    }
}
function previous() {
    if (last_post > 0) {
        last_post -= 4;
        $.post("/myposts/next_posts",
            {
                offset: last_post,
            },
            function (data, status) {
            
                var dataobj = JSON.parse(data);
                for (i = 0; i < 4; i++) {
                    if (dataobj.heading[i]) {
                        cls = ".post" + (i + 1);
                        $(cls).show();
                        id = "#heading" + (i + 1);
                        $(id).html(dataobj.heading[i]);
                        id = "#content" + (i + 1);
                        $(id).html(dataobj.content[i]);
                       
                    
                    }


                }

            },
        );
    }

}
$(document).ready(function () {
    readMore();
 });

function readMore() {
    var maxLength = 300;
    $(".show-read-more").each(function () {
        var myStr = $(this).text();
        if ($.trim(myStr).length > maxLength) {
            var newStr = myStr.substring(0, maxLength);
            var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
            $(this).empty().html(newStr);
            $(this).append(' <a href="javascript:void(0);" class="read-more">read more...</a>');
            $(this).append('<span class="more-text">' + removedStr + '</span>');
        }
    });
    $(".read-more").click(function () {
        $(this).siblings(".more-text").contents().unwrap();
        $(this).remove();
    });
    console.log('hi');
}
