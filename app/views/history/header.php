<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-signin-client_id"
        content="992378561159-8cmqpsqkjnttfk9v26a4n3f9ech04ngk.apps.googleusercontent.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Example page Test</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Material Design Bootstrap -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.0/css/mdb.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="/public/stylesheet/footer.css">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>

    <script>
    $(document).ready(function() {

        function signOut() {

            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function() {
                console.log('User signed out.');
            });
        }

        function onLoad() {
            gapi.load('auth2', function() {
                gapi.auth2.init();
            });

        }

        $('#ModalCenter').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget);
            var question = button.data('whatever');
            $('#modalTitle').text(question);
            $('#modalBody').text("Loading answer. Please wait...");
            $('#check').hide();
            $("#answerLinks").empty();
            $.post('/answer/ajax_call', {
                question: question
            }, function(data, success) {
                $('#check').show();
                var response = JSON.parse(data);
                $('#modalBody').text(response.answer);
                $.each(response.url, function(idx, element) {
                    let answerLink = $('<a class="pb-2" style="display:table; overflow:auto; word-break: break-all;" rel="noopener noreferrer" target="_blank" \
                        href="' + element + '">' + element + '</a>');
                    $("#answerLinks").append(answerLink);
                });
            });
        });

        $('#search').keyup(function() {
            var input, filter, ol, li, a, i, txtValue;
            input = document.getElementById('search');
            filter = input.value.toUpperCase();
            ol = document.getElementById("questions");
            li = ol.getElementsByTagName('li');

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                txtValue = a.textContent || a.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }

        });

    });
    </script>
</head>