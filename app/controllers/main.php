<?php

class Main extends Controller
{

    /*
     * http://localhost/
     */
    function Index()
    {

        if (!isset($_SESSION['username'])) {

            header('Location: /home');
        } else {

            header('Location: /question');
        }
    }

    /*
     * http://localhost/anothermainpage
     */
    function anotherMainPage()
    {
        echo 'Works!';
    }
}
