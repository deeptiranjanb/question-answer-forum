<?php

class History extends Controller
{

    /*
     * http://localhost/dashboard
     */
    function Index()
    {
        $this->model('questions');
        $questionHistory = $this->questions->getHistory($_SESSION['username']);
        $arr['questionHistory'] = $questionHistory;
        $this->view('history/header');
        $this->view('history/index', $arr);
        $this->view('template/footer');
    }
}
