<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-signin-client_id"
        content="992378561159-8cmqpsqkjnttfk9v26a4n3f9ech04ngk.apps.googleusercontent.com">

    <title><?php echo isset($_SESSION['username']) ? $_SESSION['username'] : "SIGN IN | QA Forum"; ?></title>

    <!-- Favicon -->
    <link rel="icon" href="favicon.png" type="image/png" sizes="32x32" />

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="/public/stylesheet/signin.css">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    <script>
    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        var Name = profile.getName();
        var id_token = googleUser.getAuthResponse().id_token;

        $.post("signin/google_sign_in", {

            id_token: id_token,
            username: Name,
        }, function(result) {
            if (result == 1) {
                window.location = "/question";
            }

        });

    }

    function renderButton() {
        gapi.signin2.render('my-signin2', {
            'scope': 'profile email',
            'width': 240,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': onSignIn,

        });
    }
    </script>

</head>