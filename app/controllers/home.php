<?php

class Home extends Controller
{

    /*
     * http://localhost/login
     */
    function Index()
    {
        $this->view('template/header');
        $this->view('home');
        $this->view('template/footer');
    }

    /*
     * http://localhost/login/log_in
     */
    function sign_in()
    {

        // Loads /models/example.php

        header("Location: /signin");
    }

    /*
     * http://localhost/login/logout
     */
    function sign_out()
    {

        $_SESSION = [];
        session_unset();
        session_destroy();
        header('Location: /');
    }
}