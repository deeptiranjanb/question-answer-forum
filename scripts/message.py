from __future__ import unicode_literals, print_function
import numpy as np
import string
import re
import codecs
import wikipedia
import datetime
import spacy
import en_core_web_lg
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.parse as urlparse
from googlesearch import search
import nltk
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from spacy.lang.en import English
import mysql.connector

nlp = spacy.load("en_core_web_lg")
ps = PorterStemmer()

# Disable displaying SSL verification warnings
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# Get the content from google links
# Utility function to pick a random user-agent
def get_random_ua():
    random_ua = None
    ua_file = "ua_file.txt"
    try:
        with open(ua_file, encoding="utf-8") as f:
            lines = f.readlines().decode("utf-8")
            if len(lines) > 0:
                prng = np.random.RandomState()
                index = prng.permutation(len(lines) - 1)
                idx = np.asarray(index, dtype=np.integer)[0]
                random_ua = lines[int(idx)]
    except Exception as ex:
        pass
    finally:
        return random_ua


# Utility function to pick a random delay


def get_random_delay():
    delay = 2.0
    try:
        random_num = np.random.uniform(2, 3)
        delay = round(random_num, 4)
    except Exception:
        pass
    finally:
        return delay


# Get top sites from google for a query


def google_search(query, num_results=None):
    def empty():  # Empty generator
        yield from ()

    results = empty()
    try:
        results = search(
            query,
            lang="en",
            start=0,
            stop=num_results,
            pause=get_random_delay(),
            user_agent=get_random_ua(),
        )
    except Exception:
        pass
    finally:
        return results


# Helper function to retrieve information directly from Wikipedia


def get_wiki_info(wiki_url):
    wiki_text = ""
    url_segments = wiki_url.rpartition("/")
    if "en.wikipedia.org" == url_segments[2]:
        return wiki_text
    try:
        wikipedia.set_lang("en")
        wikipedia.set_rate_limiting(True, min_wait=datetime.timedelta(0, 0, 50000))

        title_path = url_segments[2]
        title = urlparse.unquote(title_path)
        title = title.replace("_", " ")
        wiki_page = wikipedia.page(title)
        wiki_text = wiki_page.summary or wiki_page.content
    except (IndexError, wikipedia.exceptions.WikipediaException):
        pass
    finally:
        return wiki_text


# Retrieve information from all wiki pages


def get_all_wikis(wiki_links):
    wiki_contents = []
    for url in wiki_links:
        wiki_info = get_wiki_info(url)
        if wiki_info:
            wiki_contents.append(wiki_info)
    return wiki_contents


# Helper function to download the html page of a site


def download_site(url, session):
    html_page = ""
    user_agent = get_random_ua()
    headers = {
        "user-agent": user_agent,
    }
    try:
        with session.get(url, headers=headers, timeout=3.5, verify=False) as response:
            if response.status_code == 200:
                html_page = response.text
    except requests.exceptions.RequestException:
        pass
    finally:
        return html_page


# Retrieve html responses from all sites


def download_all_sites(sites):
    site_contents = []
    with requests.Session() as session:
        for url in sites:
            html_page = download_site(url, session)
            if html_page:
                site_contents.append(html_page)
    return site_contents


# Helper function to extract text from the web page's source code


def text_from_html(html):
    try:
        soup = BeautifulSoup(html, "lxml")
    except AttributeError as e:
        return None

    def tag_visible(element):  # Helper function to filter out futile HTML tags
        blacklist = [
            "style",
            "label",
            "[document]",
            "embed",
            "img",
            "object",
            "noscript",
            "header",
            "html",
            "iframe",
            "audio",
            "picture",
            "meta",
            "title",
            "aside",
            "footer",
            "svg",
            "base",
            "figure",
            "form",
            "nav",
            "head",
            "link",
            "button",
            "source",
            "canvas",
            "br",
            "input",
            "script",
            "wbr",
            "video",
            "param",
            "hr",
            "a",
        ]
        if element.parent.name in blacklist:
            return False
        if isinstance(element, Comment):
            return False
        return True

    texts = soup.body.findAll(text=True)
    visible_texts = filter(tag_visible, texts)
    return " ".join(t.strip() for t in visible_texts)


# Extract textual content from all pages


def extract_text(extracted_site_contents):
    text_contents = []
    for html in extracted_site_contents:
        page_text = text_from_html(html)
        if page_text is not None:
            text_contents.append(page_text)
    return text_contents


# Get a list of relevant text documents for the input query


def fetch_text_results(query):
    text_results = []
    links = []
    wiki_links, other_sites = [], []
    # Search Google
    sites = google_search(query, num_results=2)  # Obtain the top 2 URLs
    # URL list generation and bifurcation
    for url in sites:  # Split the URLs into Wiki links and site links
        if "gstatic.com" in url:  # Exclude static google content
            continue
        target = wiki_links if "en.wikipedia.org" in url else other_sites
        target.append(url)
        links.append(url)
    # Fetching Wiki pages
    wiki_pages = get_all_wikis(wiki_links)
    if wiki_pages:
        text_results += wiki_pages  # Merge wikis with text results
    # Fetching site HTMLs and extracting text
    site_pages = download_all_sites(other_sites)  # Get HTML from site URLs
    if site_pages:  # Emptiness check
        page_texts = extract_text(site_pages)  # Extract texts from HTML
        if page_texts:  # Emptiness check
            text_results += page_texts  # Merge site texts with text results
    return (text_results, links)


# Get an agglomerated string from multiple relevant documents for a query


def fetch_merged_results(query):
    results_list, links = fetch_text_results(query)
    results_string = " ".join(results_list)  # Join the list into a string
    return (results_string, links)


# This function splits the content into array of sentences
def sentenceSplit(sentence):
    nlpSentence = English()
    nlpSentence.add_pipe(nlpSentence.create_pipe("sentencizer"))
    doc = nlpSentence(sentence)
    sentenceList = [sent.string.strip() for sent in doc.sents]
    return sentenceList


# This function is used for punctuation removal,
# Wh-question removal, stop-word removal and,
# capitalizing words in the user query


def stopWord(query):
    # Punctuation removal
    query = query.translate(str.maketrans("", "", string.punctuation))

    # Wh-question removal
    queryArray = query.split()
    reg = re.findall(r"\b[wW]h\w+|how|How|define|Define|Explain|explain", query)
    for i in reg:
        if i in queryArray:
            queryArray.remove(i)
    query = ""
    for i in queryArray:
        query += i + " "

    # stop word removal
    stopWords = set(stopwords.words("english"))
    wordTokens = word_tokenize(query)
    filteredSentence = [w for w in wordTokens if not w in stopWords]
    stringText = ""
    for i in filteredSentence:
        stringText += i + " "

    # capitalize the words
    queryArray = stringText.split()
    output = ""
    for i in queryArray:
        i = i.capitalize()
        output += i + " "
    return output


# This function extracts answer from the search text
# depending on the type of question tag
def ruleBasedModel(
    quesTag, queryNer, searchTextNer, searchText, fileContent, listQuesTag, links
):
    answerArray = []
    answer = ""
    # For who and whom type of questions
    if quesTag == "who" or quesTag == "Who" or quesTag == "whom" or quesTag == "Whom":
        flag = False
        for i in queryNer.ents:
            if i.label_ == "PERSON":
                flag = True
        if flag == True:
            answer = searchText
        else:
            for i in searchTextNer.ents:
                if (
                    i.label_ == "PERSON"
                    or i.label_ == "ORG"
                    and i.text not in answerArray
                ):
                    answerArray.append(i.text)
            if len(answerArray) == 1:
                answer = answerArray[0]
            else:
                answer = searchText

    # For 'When' questions
    elif quesTag == "when" or quesTag == "When":
        for i in searchTextNer.ents:
            if i.label_ == "DATE" and i.text not in answerArray:
                answerArray.append(i.text)
        if len(answerArray) == 1:
            answer = answerArray[0]
        else:
            answer = searchText

    # For 'Where' questions
    elif quesTag == "where" or quesTag == "Where":
        for i in searchTextNer.ents:
            for j in queryNer.ents:
                if i.label_ == "GPE" and i.text != j.text and i.text not in answerArray:
                    answerArray.append(i.text)
        if len(answerArray) == 1:
            answer = answerArray[0]
        else:
            answer = searchText

    # For 'What' and 'Which' questions
    elif (
        quesTag == "which"
        or quesTag == "Which"
        or quesTag == "What"
        or quesTag == "what"
    ):
        answer = searchText

    # For "How" question tag
    elif quesTag == "how" or quesTag == "How":
        answer = searchText

    # For Default case
    elif quesTag not in listQuesTag:
        answer = searchText

    answer += "@@@@@@"
    answer += "$$$$$$$$".join(links)
    return answer


# This function helps in searching relevant sentences
# from the online scraped content depending on the
# question tag found in user query
def searchingText(query, fileContent, queryNer, quesTag, listQuesTag):
    # Splitting the file content into sentences
    listFileContent = sentenceSplit(fileContent)
    universal = ""
    # For who question type
    if quesTag == "who" or quesTag == "Who" or quesTag == "whom" or quesTag == "Whom":
        l = []
        fileContent1 = ""
        l1 = ["current", "ongoing", "latest", "today"]
        date = []
        dateFlag = False
        for i in queryNer.ents:
            if i.label_ == "DATE":
                date.append(i.text)
                dateFlag = True
        if dateFlag:
            for i in date:
                for k in range(len(listFileContent)):
                    if (
                        i in listFileContent[k]
                        and listFileContent[k] not in fileContent1
                    ):
                        # print("Found in text: ",i)
                        fileContent1 += listFileContent[k]
        else:
            for i in l1:
                for k in range(len(listFileContent)):
                    if (
                        (i in listFileContent[k])
                        or (i.capitalize() in listFileContent[k])
                        and listFileContent[k] not in fileContent1
                    ):
                        # print("Found in text: ",i)
                        fileContent1 += listFileContent[k]
        k = 0
        if fileContent1 == "":
            for i in query.split():
                for k in range(len(listFileContent)):
                    x = i.lower()
                    if (
                        (
                            (i in listFileContent[k])
                            or (x in listFileContent[k])
                            or (ps.stem(i) in listFileContent[k])
                            or (ps.stem(x) in listFileContent[k])
                        )
                        and k not in l
                        and listFileContent[k] not in universal
                    ):
                        l.append(k)
                        universal += listFileContent[k]
        else:
            for i in query.split():
                for k in range(len(listFileContent)):
                    x = i.lower()
                    if (
                        ((i in listFileContent[k]) or (x in listFileContent[k]))
                        and k not in l
                        and listFileContent[k] not in universal
                    ):
                        l.append(k)
                        universal += listFileContent[k]

            listFileContent1 = sentenceSplit(fileContent1)

            for i in listFileContent1:
                if i not in universal:
                    universal += i

    # For when question tags
    elif quesTag == "when" or quesTag == "When":
        l = []
        relevantString = ""
        for i in query.split():
            for j in range(len(listFileContent)):
                if (
                    ((i in listFileContent[j]) or (i.lower() in listFileContent[j]))
                    and j not in l
                    and listFileContent[j] not in relevantString
                ):
                    l.append(j)
                    relevantString += listFileContent[j]

        relevantStringArray = sentenceSplit(relevantString)

        for i in relevantStringArray:
            x = nlp(i)
            for j in x.ents:
                if j.label_ == "DATE":
                    universal += i
                    break

    # For where type of questions
    elif quesTag == "where" or quesTag == "Where":
        l = []
        relevantString = ""
        for i in query.split():
            for j in range(len(listFileContent)):
                if (
                    ((i in listFileContent[j]) or (i.lower() in listFileContent[j]))
                    and j not in l
                    and listFileContent[j] not in relevantString
                ):
                    l.append(j)
                    relevantString += listFileContent[j]

        relevantStringArray = sentenceSplit(relevantString)

        for i in relevantStringArray:
            x = nlp(i)
            for j in x.ents:
                if j.label_ == "GPE":
                    universal += i
                    break

    # For which and what type of question
    elif (
        quesTag == "which"
        or quesTag == "Which"
        or quesTag == "What"
        or quesTag == "what"
    ):
        l = []
        for i in query.split():
            for j in range(len(listFileContent)):
                if (
                    ((i in listFileContent[j]) or (i.lower() in listFileContent[j]))
                    and j not in l
                    and listFileContent[j] not in universal
                ):
                    l.append(j)
                    universal += listFileContent[j]

        dateArray = []
        dateFlag = False
        for i in queryNer.ents:
            if i.label_ == "DATE":
                dateArray.append(i.text)
                dateFlag = True

        universalArray = sentenceSplit(universal)
        dateRelevantString = ""

        if dateFlag:
            for i in dateArray:
                for k in range(len(universalArray)):
                    if (
                        i in universalArray[k] or i.lower() in universalArray[k]
                    ) and universalArray[k] not in dateRelevantString:
                        dateRelevantString += universalArray[k]
            universal = dateRelevantString

    # For how and default case
    elif quesTag == "how" or quesTag == "How" or quesTag not in listQuesTag:
        l = []
        for i in query.split():
            for j in range(len(listFileContent)):
                if (
                    ((i in listFileContent[j]) or (i.lower() in listFileContent[j]))
                    and j not in l
                    and listFileContent[j] not in universal
                ):
                    l.append(j)
                    universal += listFileContent[j]

    return universal


# This function extracts question tag from user query
def questionTag(query):
    quesTag = ""
    listQuesTag = [
        "what",
        "What",
        "who",
        "Who",
        "when",
        "When",
        "where",
        "Where",
        "which",
        "Which",
        "how",
        "How",
        "Whom",
        "whom",
    ]
    regeX = re.findall(r"\b[wW]h\w+|how|How", query)
    for i in regeX:
        quesTag += i
    return quesTag, listQuesTag


# Main function
def answer_to(query):
    mydb = mysql.connector.connect(
        host="localhost", user="root", passwd="780952@Drb", database="qaForum"
    )

    answer = "Sorry. No data available!"
    corpus, links = fetch_merged_results(query)
    if not corpus:
        return answer
    listQuery = query.split()
    quesTag, listQuesTag = questionTag(query)
    # print("Ques tag: ",quesTag)
    query = stopWord(query)
    # print("Refined query: ",query)
    queryNer = nlp(query)
    # for w in queryNer.ents:
    #   x_ = w.label_
    #   y_ = w.text
    #   print(x_,y_,end="\n")
    searchText = searchingText(query, corpus, queryNer, quesTag, listQuesTag)

    # print("Search text: ",searchText)
    searchTextNer = nlp(searchText)
    # for w in searchTextNer.ents:
    #   x_ = w.label_
    #   y_ = w.text
    #   print(x_,y_,end="\n")
    answer = ruleBasedModel(
        quesTag, queryNer, searchTextNer, searchText, corpus, listQuesTag, links
    )

    return answer


if __name__ == "__main__":
    question = input("enter question")
    print(answer_to(question))

