import grpc
from concurrent import futures
import time
from dotenv import load_dotenv
import os

# import the generated classes
import message_pb2
import message_pb2_grpc

# import the message.py
import message

# create a class to define the server functions, derived from
# message_pb2_grpc.MessageingServiceServicer
class MessageingServicer(message_pb2_grpc.MessageingServiceServicer):

    # calculator.sum is exposed here
    def generateAnswer(self, request, context):

        reply = message_pb2.Answer()
        reply.answer = message.answer_to(request.question)
        print("Question:", request.question)
        # print("Answer:", reply.answer)
        return reply


# create a gRPC server
server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

# use the generated function `add_MessageingServiceservicer_to_server`
# to add the defined class to the server
message_pb2_grpc.add_MessageingServiceServicer_to_server(MessageingServicer(), server)
# listen on port 50051
load_dotenv()
portno = os.getenv("PORT_NO")

print("Starting server. Listening on port %s." % (portno))
server.add_insecure_port("[::]:" + portno)
server.start()

# since server.start() will not block,
# a sleep-loop is added to keep alive
try:
    while True:
        time.sleep(86400)
except KeyboardInterrupt:
    server.stop(0)
