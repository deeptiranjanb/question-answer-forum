<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Message;

/**
 */
class MessageingServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Message\Question $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function generateAnswer(\Message\Question $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/message.MessageingService/generateAnswer',
        $argument,
        ['\Message\Answer', 'decode'],
        $metadata, $options);
    }

}
