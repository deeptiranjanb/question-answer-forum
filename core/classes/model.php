<?php

abstract class Model
{

    protected $db;

    function __construct()
    {

        global $app;

        $this->db = $app->db;
    }
    function insert($tableName, $columns)
    {
        $sql = "INSERT INTO " . $tableName . "(" . implode(",", array_keys($columns)) . ") VALUES (:" . implode(", :", array_keys($columns)) . ")";
        $statement = $this->db->prepare($sql);
        return $statement->execute($columns);
    }
    public function select($tableName, $conditions)
    {

        $sql = "SELECT ";
        $sql .= array_key_exists("select", $conditions) ? implode(', ', $conditions["select"]) : '*';
        $sql .= " FROM ";

        if (is_array($tableName)) {
            $sql .= implode(" join ", $tableName);
            if (array_key_existS("where", $conditions)) {
                $sql .= " WHERE ";
                $i = 0;
                foreach ($conditions["where"] as $keys => $values) {
                    $sql .= $keys . " = " . $values;
                    if (array_key_exists("operators", $conditions) and $i < count($conditions["operators"])) {
                        $sql .= ' ' . $conditions["operators"][$i] . ' ';
                        $i++;
                    }
                }

                $statement = $this->db->prepare($sql);
                $statement->execute();
                return $statement->fetchAll();
            }
        } else {
            $sql .= $tableName;

            if (array_key_exists("where", $conditions)) {
                $sql .= " WHERE ";
                $i = 0;

                foreach (array_keys($conditions["where"]) as $keys) {
                    $sql .= $keys;
                    $sql .= " = :" . $keys;

                    if (array_key_exists("operators", $conditions) and $i < count($conditions["operators"])) {
                        $sql .= ' ' . $conditions["operators"][$i] . ' ';
                        $i++;
                    }
                }
            }
            if (array_key_exists("limit", $conditions)) {
                $sql .= " LIMIT ";
                $sql .= implode(',', $conditions['limit']);
            }
            $statement = $this->db->prepare($sql);
            if (array_key_exists("where", $conditions)) {

                $statement->execute($conditions["where"]);
                return $statement->fetchAll();
            }
            $statement->execute();
            return $statement->fetchAll();
        }
    }
}
