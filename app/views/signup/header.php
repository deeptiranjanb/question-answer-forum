<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo isset($_SESSION['username']) ? $_SESSION['username'] : "SIGN UP | QA Forum"; ?></title>

    <!-- Favicon -->
    <link rel="icon" href="favicon.png" type="image/png" sizes="32x32" />

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="/public/stylesheet/signup.css">
    <link rel="stylesheet" type="text/css" href="/public/stylesheet/footer.css">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

    <script>
    $(document).ready(function() {
        $("#username").blur(function() {
            if ($("#username").val() == '') {
                $("#usernameErr").html("username cannot be empty");
            } else {

                $.post("/signup/username_availability", {
                        username: $("#username").val(),
                    },
                    function(data, status) {
                        if (data) {
                            $("#usernameErr").css("color", "green");
                            $("#usernameErr ").html("username available");
                        } else {
                            $("#usernameErr").css("color", "red");
                            $("#usernameErr ").html("username unavailable");

                        }

                    });
            }
        });

    });
    </script>

</head>