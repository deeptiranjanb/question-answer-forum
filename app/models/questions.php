<?php

/*
 * Every class derriving from Model has access to $this->db
 * $this->db is a PDO object
 * Has a config in /core/config/database.php
 */
class Questions extends Model
{

    function insertQuestions($questionDetails)
    {
        try {
            if ($this->verifyQuestion($questionDetails['question'])) {
                $result = $this->insert('questions', $questionDetails);
                try {
                    $user_id = $this->getUserId($_SESSION['username']);
                    $question_id = $this->getQuestionId($questionDetails['question']);
                    $condition = array(
                        'user_id' => $user_id,
                        'question_id' => $question_id,
                    );
                    $result = $this->insert('userQuestions', $condition);
                } catch (Exception $e) {
                    echo "question could not be inserted: " . $e;
                }
            } else {
                try {
                    $user_id = $this->getUserId($_SESSION['username']);
                    $question_id = $this->getQuestionId($questionDetails['question']);
                    $condition = array(
                        'user_id' => $user_id,
                        'question_id' => $question_id,
                    );
                    if ($this->verifyUserQuestion($condition)) {
                        $result = $this->insert('userQuestions', $condition);
                    }
                } catch (Exception $e) {
                    echo "question could not be inserted: " . $e;
                }
            }
        } catch (Exception $e) {
            echo "question could not be inserted";
        }
        return $result;
    }

    function getUserId($username)
    {
        $conditions = array(
            "select" => ['id'],
            "where" => array(
                'username' => $username
            ),
        );
        try {
            $result = $this->select('users', $conditions);
        } catch (Exception $e) {
            echo "no user was selected";
        }
        return $result[0]['id'];
    }

    function getQuestionId($question)
    {
        $conditions = array(
            "select" => ['id'],
            "where" => array(
                'question' => $question
            ),
        );
        try {
            $result = $this->select('questions', $conditions);
        } catch (Exception $e) {
            echo "no question was selected";
        }
        return $result[0]['id'];
    }

    function verifyQuestion($question)
    {
        $conditions = array(
            "select" => ['id'],
            "where" => array(
                'question' => $question
            ),
        );
        try {
            $result = $this->select('questions', $conditions);
        } catch (Exception $e) {
            echo "no question was selected";
        }

        if (count($result) == 0) {
            return true;
        } else {
            return false;
        }
    }
    function verifyUserQuestion($condition)
    {
        $conditions = array(
            "select" => ['id'],
            "where" => array(
                'user_id' => $condition['user_id'],
                'question_id' => $condition['question_id']
            ),
            "operators" => ["AND"],
        );
        try {
            $result = $this->select('userQuestions', $conditions);
        } catch (Exception $e) {
            echo "no question was selected";
        }

        if (count($result) == 0) {
            return true;
        } else {
            return false;
        }
    }

    function getHistory($username)
    {
        $user_id = $this->getUserId($username);
        $conditions = array(
            "select" => ["question"],
            "where" => array(
                "questions.id" => "userQuestions.question_id",
                "users.id" => "userQuestions.user_id",
                "users.username" => "'" . $username . "'"
            ),
            "operators" => ["AND", "AND"]
        );

        $tableName = ["users", "userQuestions", "questions"];
        try {
            $result = $this->select($tableName, $conditions);
        } catch (Exception $e) {
            echo $e;
        }
        $questionHistory = [];
        foreach ($result as $k => $v) {
            array_push($questionHistory, $v['question']);
        }
        return $questionHistory;
    }
}
