<?php

class Signin extends Controller
{
    function Index()
    {
        if (!isset($_SESSION['username'])) {
            $this->view('signin/header');
            $this->view('signin/index');
            $this->view('template/footer');
        } else {
            header('Location: /questions');
        }
    }

    function Sign_Up()
    {
        header('Location: /signup');
    }

    function Sign_In()
    {
        if (!empty($_POST["token"])) {
            if (hash_equals($_SESSION["token"], $_POST["token"])) {
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $this->model("user");
                    foreach ($_POST as $k => $v) {
                        $userdetails[$k] = $this->check_input($v);
                    }

                    if ($this->user->validateUser($userdetails)) {
                        $_SESSION['username'] = $userdetails['username'];
                        header("Location: /question");
                    } else {
                        $err["error"] = "username or password is not valid";
                        $this->view('signin/header');
                        $this->view('signin/index', $err);
                        $this->view('template/footer');
                    }
                }
            }
        }
    }

    function google_Sign_In()
    {
        require __DIR__ . "/../../vendor/autoload.php";

        // Get $id_token via HTTPS POST.
        $CLIENT_ID = "992378561159-8cmqpsqkjnttfk9v26a4n3f9ech04ngk.apps.googleusercontent.com";

        $client = new Google_Client([
            'client_id' => $CLIENT_ID
        ]);  // Specify the CLIENT_ID of the app that accesses the backend

        $payload = $client->verifyIdToken($_POST['id_token']);

        if ($payload) {
            $userid = $payload['sub'];
            // If request specified a G Suite domain:
            //$domain = $payload['hd'];

            $userdetails = array(
                'username' =>  substr($payload['email'], 0, strpos($payload['email'], "@")),
                'password' =>  substr($payload['email'], 0, strpos($payload['email'], "@")),
                'name' => $payload['name'],
                'email' => $payload['email'],
            );

            $hash = password_hash($userdetails['password'], PASSWORD_DEFAULT);
            $userdetails['password'] = $hash;

            $this->model("user");
            if (
                $this->user->verifyEmail($userdetails['email'])
                && $this->user->verifyUsername($userdetails['username'])
            ) {

                if ($this->user->addUser($userdetails)) {
                    $_SESSION['username'] = substr($payload['email'], 0, strpos($payload['email'], "@"));
                    echo 1;
                }
            } else {

                $_SESSION['username'] = substr($payload['email'], 0, strpos($payload['email'], "@"));
                echo 1;
            }
        } else {
            // Invalid ID token
            echo 0;
        }
    }
}