<?php
require __DIR__ . "../../../controllers/csrf.php";
?>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top">
        <div class="container">
            <a class="navbar-brand" href="/question">QA Forum</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarResponsive">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active" id="search">
                        <a class="nav-link" href="/question">Search
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/history">History</a>
                    </li>
                    <li class="nav-item" id="service">
                        <a class="nav-link" href="#services" onclick="service()">Services</a>
                    </li>
                    <li class="nav-item avatar dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg"
                                class="rounded-circle z-depth-0" style="height:30px" alt="avatar image">
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                            aria-labelledby="navbarDropdownMenuLink-55">
                            <a class="dropdown-item" href="#">My account</a>
                            <a class="dropdown-item" onclick="signOut()">Sign out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Full Page Image Header with Vertically Centered Content -->
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 text-center ">
                    <h1 class="font-weight-light">Question Answering Forum</h1>
                    <p class="lead">Ask us a quesiton</p>
                    <br />
                    <form class="form-inline justify-content-center" action="/answer" method="post">
                        <input type="search" name="question"><i class="fa fa-search"></i>
                        <input type="hidden" name="token" value="<?php echo $token; ?>">

                    </form>
                </div>
            </div>
    </header>


    <!-- Page Content -->
    <section class="py-5">
        <div class="container" id="services">
            <h2 class="font-weight-light">Services</h2>
            <p>Currently we only answer your questions. More services coming soon...</p>
        </div>
    </section>