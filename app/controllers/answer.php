<?php
//load the generated classes from output folder
include __DIR__ . '/../../vendor/autoload.php';
include __DIR__ . '/../../out/Message/MessageingServiceClient.php';
include __DIR__ . '/../../out/Message/Question.php';
include __DIR__ . '/../../out/Message/Answer.php';
include __DIR__ . '/../../out/GPBMetadata/Message.php';

class Answer extends Controller
{
    function Index()
    {
        if (!isset($_SESSION['username'])) {

            header('Location: /signin');
        } else if (!isset($_POST['question'])) {
            header('Location: /question');
        } else {
            try {
                $response = $this->client();
                list($answer, $links) = explode('@@@@@@', $response);
                $url = explode('$$$$$$$$', $links);
                $this->Ask_Question();
            } catch (Exception $e) {
                echo $e;
            }
            $question = $this->check_input($_POST['question']);
            $response = array(
                'question' => $question,
                'answer' => $answer,
                'url' => $url
            );
            $this->view('template/header');
            $this->view('answer/index', $response);
            $this->view('template/footer');
        }
    }

    function Ask_Question()
    {
        if (!empty($_POST["token"])) {

            if (hash_equals($_SESSION["token"], $_POST["token"])) {

                if ($_SERVER["REQUEST_METHOD"] == "POST") {

                    $this->model("questions");
                    $questionDetails['question'] = $this->check_input(($_POST['question']));
                    $this->questions->insertQuestions($questionDetails);
                }
            }
        }
    }

    function client()
    {
        // Listening to port
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . "/../../");
        $dotenv->load();
        $portno = getenv('PORT_NO');

        $client = new \Message\MessageingServiceClient('localhost:' . $portno, [
            'credentials' => \Grpc\ChannelCredentials::createInsecure(),
        ]);

        try {
            // request object
            $request = new \Message\Question();
            // Question
            $request->setQuestion((string) $this->check_input($_POST['question']));

            list($res, $status) = $client->generateAnswer($request)->wait();

            // answer
            return $res->getAnswer();
        } catch (Exception $error) {
            echo $error;
        }
    }

    function ajax_call()
    {
        $response = $this->client();
        list($answer, $links) = explode('@@@@@@', $response);
        $url = explode('$$$$$$$$', $links);
        $response = array(
            'answer' => $answer,
            'url' => $url
        );
        echo json_encode($response);
    }
}
