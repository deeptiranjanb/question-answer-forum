<body class="d-flex flex-column">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow sticky-top">
        <div class="container">
            <a class="navbar-brand" href="/question">QA Forum</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <form class="d-flex pt-2 pt-lg-0" role="search">
                    <div class="input-group">
                        <input type="text" id="search" class="form-control" placeholder="Search">

                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">
                                <span class="fa fa-search">
                                    <span class="sr-only">Search</span>
                                </span>
                            </button>
                        </div>
                    </div>
                </form>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item" id="search">
                        <a class="nav-link" href="/question">Search </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/history">History
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item avatar dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg" class="rounded-circle z-depth-0" style="height:30px" alt="avatar image">
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55">
                            <a class="dropdown-item" href="#">My account</a>
                            <a class="dropdown-item" href="/home/sign_out" onclick="signOut();">Sign out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container" id="page-content">
        <div class="card border-2 shadow my-5">
            <div class="card-body-lg p-5">
                <h3 class="font-weight-light">Questions History</h3>
                <hr class="my-4">
                <div>
                    <ol id="questions">
                        <?php
                        for ($i = 0; $i < count($questionHistory); $i++) {
                        ?> <div class="d-block my-4" data-toggle="modal" id="<?php echo $i; ?>"" data-target=" #ModalCenter" data-whatever="<?php echo $questionHistory[$i]; ?>" data-body="you will get your answer" role='button'>
                                <li> <a href="#"><?php echo $questionHistory[$i]; ?> </a></li>
                            </div>
                        <?php
                        }
                        ?>

                    </ol>
                </div>
            </div>
            <div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title lead" id="modalTitle">question</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="modalBody">
                            Unable to connect to server. Please try again after sometime.
                        </div>
                        <div class="modal-body" id="check">
                            Not sure what you are looking for. check these links below to know more:
                        </div>
                        <div class="modal-body" id="answerLinks">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>