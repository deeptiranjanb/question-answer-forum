# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: message.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='message.proto',
  package='message',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=b'\n\rmessage.proto\x12\x07message\"\x1c\n\x08Question\x12\x10\n\x08question\x18\x01 \x01(\t\"\x18\n\x06\x41nswer\x12\x0e\n\x06\x61nswer\x18\x01 \x01(\t2K\n\x11MessageingService\x12\x36\n\x0egenerateAnswer\x12\x11.message.Question\x1a\x0f.message.Answer\"\x00\x62\x06proto3'
)




_QUESTION = _descriptor.Descriptor(
  name='Question',
  full_name='message.Question',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='question', full_name='message.Question.question', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=26,
  serialized_end=54,
)


_ANSWER = _descriptor.Descriptor(
  name='Answer',
  full_name='message.Answer',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='answer', full_name='message.Answer.answer', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=56,
  serialized_end=80,
)

DESCRIPTOR.message_types_by_name['Question'] = _QUESTION
DESCRIPTOR.message_types_by_name['Answer'] = _ANSWER
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Question = _reflection.GeneratedProtocolMessageType('Question', (_message.Message,), {
  'DESCRIPTOR' : _QUESTION,
  '__module__' : 'message_pb2'
  # @@protoc_insertion_point(class_scope:message.Question)
  })
_sym_db.RegisterMessage(Question)

Answer = _reflection.GeneratedProtocolMessageType('Answer', (_message.Message,), {
  'DESCRIPTOR' : _ANSWER,
  '__module__' : 'message_pb2'
  # @@protoc_insertion_point(class_scope:message.Answer)
  })
_sym_db.RegisterMessage(Answer)



_MESSAGEINGSERVICE = _descriptor.ServiceDescriptor(
  name='MessageingService',
  full_name='message.MessageingService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=82,
  serialized_end=157,
  methods=[
  _descriptor.MethodDescriptor(
    name='generateAnswer',
    full_name='message.MessageingService.generateAnswer',
    index=0,
    containing_service=None,
    input_type=_QUESTION,
    output_type=_ANSWER,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_MESSAGEINGSERVICE)

DESCRIPTOR.services_by_name['MessageingService'] = _MESSAGEINGSERVICE

# @@protoc_insertion_point(module_scope)
